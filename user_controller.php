<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class User extends MY_Controller
{
    
    public function __construct()
   {
        parent::__construct();
        $this->load->library('upload');
   }

    public function index()
    {
        $this->data['user'] = $this->user;
        $this->data['mainContent'] = $this->load->view('user/index', $this->data, true);
        $this->load->view('layout/main', $this->data);
    }

    public function login()
    {
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[6]');

        if($this->session->userdata('id')) // Al ingelogd
            redirect('/');

        if ($this->form_validation->run() !== false)
        {

            $this->load->model('user_model');
            $user = $this->user_model->checkUser($this->input->post('email'), $this->input->post('password'));

            if ($user)
            {
                $this->session->set_userdata('id', $user->id);                
                redirect('/');
            }
            else
            {
                $this->form_validation->set_message('email', 'Ongeldige gebruikersnaam of wachtwoord');
            }

        }
        
        $this->data['mainContent'] = $this->load->view('user/login', null, true);
        $this->load->view('layout/main', $this->data);
    }
    
    public function deleteProfile()
    {

        if(!$this->session->userdata('id')) // Niet ingelogd
            redirect('/');

        if ($this->input->post('delete'))
        {

            $this->load->model('user_model');
            if(!empty($this->user->photo))
            {
                unlink('./photos/thumb/'.$this->user->photo);
                unlink('./photos/'.$this->user->photo); 
            }            
            $user = $this->user_model->delete($this->user->id);

            $this->logout();

        }
        
        $this->data['mainContent'] = $this->load->view('user/delete', null, true);
        $this->load->view('layout/main', $this->data);
    }
    
    public function view()
    {
        $uid = (int) $this->uri->segment(3, 0);
        $this->load->model('user_model');
        
        if($uid > 0)
        {
            if($this->session->userdata('id')) // If logged in
            {
                if($uid == $this->session->userdata('id')) // If we're viewing our own profile
                    $this->data['sameUser'] = true;
            }
            
			// Retrieve all brands
            $allBrands = $this->db->get('brands')->result();
            foreach($allBrands as $b)
                $brands[$b->id] = $b->name;
                    
            $this->data['brands'] = $brands;
            
			//Retrieve user's brands
            $userBrands = $this->user_model->getBrands($uid);
            foreach($userBrands as $b)
                $uBrands[] = $b->brand;
                
            $this->data['userBrands'] = $uBrands;
            
            $this->data['userInfo'] = $this->user_model->get($uid, $this->session->userdata('id'));
            
			// Like button pressed
            if($this->input->post('like') && $this->data['userInfo'] && $this->session->userdata('id'))
                if(!$this->data['userInfo']['sessLikes'] && !$this->data['sameUser']) // If not liked yet
                {
                    $this->user_model->addLike($this->session->userdata('id'), $uid);
                    $this->data['userInfo']['sessLikes'] = $this->session->userdata('id');
                }
            
            if($this->data['userInfo']) {
                $this->data['mainContent'] = $this->load->view('user/view', $this->data, true);
                $this->load->view('layout/main', $this->data);
            }
            else {
                show_404('We konden de opgevraagde gebruiker niet vinden.');
            }
        }
        else {
            show_404('We konden de opgevraagde gebruiker niet vinden.');
        }
    }

    public function logout()
    {
        $this->session->sess_destroy();    
        redirect('/');
    }

    public function register()
    {
		if($this->session->userdata('id')) // Already logged in
            redirect('/');
        
		$this->form_validation->set_rules('nickName', 'Nickname', 'required');
		$this->form_validation->set_rules('fullName', 'Naam', 'required');
		$this->form_validation->set_rules('password', 'Wachtwoord', 'required|min_length[6]');
		$this->form_validation->set_rules('sex', 'Geslacht', 'required');		
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
		$this->form_validation->set_rules('birthDate', 'Geboortedatum', 'required');
		$this->form_validation->set_rules('description', 'Beschrijving', 'required');
        $this->form_validation->set_rules('sexPreference', 'Voorkeur', 'required');
        $this->form_validation->set_rules('minAge', 'Minimum leeftijd', 'required|greater_than[17]|less_than[101]');
        $this->form_validation->set_rules('maxAge', 'Maximum leeftijd', 'required|greater_than[17]|less_than[101]');
        for($i = 1; $i < 20; $i++)
            $this->form_validation->set_rules('q'.$i, 'Persoonlijkheids vraag '.$i, 'required|greater_than[0]|less_than[4]');
        
		// Retrieve all brands
		$allBrands = $this->db->get('brands')->result();
        foreach($allBrands as $b)
            $brands[$b->id] = $b->name;

        $this->data['brands'] = $brands;
        
        if ($this->form_validation->run() !== false)
        {

            if($this->_photoUpload())
                $loc = $this->_photoResize();
            
			$I = $N = $T = $J = 50;
			// Calculate the results of the personality test
            for($c = 1; $c <= 5; $c++)
            {
                if($this->input->post('q'.$c) == 1) $I -= 10;
                elseif($this->input->post('q'.$c) == 2) $I += 10;
            }
            for($c = 6; $c <= 9; $c++)
            {
                if($this->input->post('q'.$c) == 1) $N += 12.5;
                elseif($this->input->post('q'.$c) == 2) $N -= 12.5;
            }
            for($c = 10; $c <= 13; $c++)
            {
                if($this->input->post('q'.$c) == 1) $T += 12.5;
                elseif($this->input->post('q'.$c) == 2) $T -= 12.5;
            }
            for($c = 14; $c <= 19; $c++)
            {
                if($this->input->post('q'.$c) == 1) $J += 8.3333;
                elseif($this->input->post('q'.$c) == 2) $J -= 8.3333;
            }

            $data = array(
    			'nickName' => $this->input->post('nickName'),
    			'email' => $this->input->post('email'),
    			'fullName' => $this->input->post('fullName'),
    			'password' => sha1($this->input->post('password')),
    			'sex' => $this->input->post('sex'),
    			'birthDate' => $this->input->post('birthDate'),
    			'description' => $this->input->post('description'),
    			'photo' => $loc ? $loc : null,
    			'minAge' => $this->input->post('minAge'),
    			'maxAge' => $this->input->post('maxAge'),
    			'sexPreference' => $this->input->post('sexPreference'),
				'I' => $I,
				'N' => $N,
				'T' => $T,
				'J' => $J,
				'pref_I' => 100-$I,
                'pref_N' => 100-$N,
                'pref_T' => 100-$T,
                'pref_J' => 100-$J
			);
			
            $this->load->model('user_model');
            $user = $this->user_model->register($data, $this->input->post('brands'));
            $this->data['success'] = true;
        }
        
        $this->data['mainContent'] = $this->load->view('user/register', $this->data, true);
        $this->load->view('layout/main', $this->data);
    }
    
    public function edit()
    {
        $this->load->model('user_model');
        
        $this->form_validation->set_rules('nickName', 'Nickname', 'required');
		$this->form_validation->set_rules('fullName', 'Naam', 'required');
		$this->form_validation->set_rules('password', 'Wachtwoord', 'min_length[6]');
		$this->form_validation->set_rules('sex', 'Geslacht', 'required');		
        $this->form_validation->set_rules('email', 'Email', 'valid_email|is_unique[users.email]');
		$this->form_validation->set_rules('birthDate', 'Geboortedatum', 'required');
		$this->form_validation->set_rules('description', 'Beschrijving', 'required');
        $this->form_validation->set_rules('sexPreference', 'Voorkeur', 'required');
        $this->form_validation->set_rules('minAge', 'Minimum leeftijd', 'required|greater_than[17]|less_than[101]');
        $this->form_validation->set_rules('maxAge', 'Maximum leeftijd', 'required|greater_than[17]|less_than[101]');
        
        if($this->session->userdata('id')) // If logged in
        {
            $this->data['user'] = $this->user;
            
			// Retrieve all brands
            $allBrands = $this->db->get('brands')->result();
            foreach($allBrands as $b)
                $brands[$b->id] = $b->name;
                    
            $this->data['brands'] = $brands;
            
			
			// Retrieve user's brands
            $userBrands = $this->user_model->getBrands($this->user->id);
            
            $uBrands = array();
            foreach($userBrands as $b)
                $uBrands[$b->brand] = $b->brand;
                
            $this->data['userBrands'] = $uBrands;
                
            if ($this->form_validation->run() !== false)
            {
                if($this->_photoUpload())
                    $loc = $this->_photoResize();
                
				// Delete old photo if new one successfully uploaded
                if(($loc || $this->input->post('deletePhoto')) && !empty($this->user->photo))
                {
                    unlink('./photos/thumb/'.$this->user->photo);
                    unlink('./photos/'.$this->user->photo); 
                    if($this->input->post('deletePhoto'))
                        $this->user->photo = null;
                }
                
                $updatedData = array('nickName' => $this->input->post('nickName'),
                        			'email' => $this->input->post('email') ? $this->input->post('email') : $this->user->email,
                        			'fullName' => $this->input->post('fullName'),
                        			'password' => $this->input->post('password') ? sha1($this->input->post('password')) : $this->user->password,
                        			'sex' => $this->input->post('sex'),
                        			'birthDate' => $this->input->post('birthDate'),
                        			'description' => $this->input->post('description'),
                                    'photo' => $loc ? $loc : $this->user->photo,
                        			'minAge' => $this->input->post('minAge'),
                        			'maxAge' => $this->input->post('maxAge'),
                        			'sexPreference' => $this->input->post('sexPreference'));
            
                $this->user_model->update($this->session->userdata('id'), $updatedData, $this->input->post('brands'));
                $this->data['success'] = true;
            }
            
            $this->data['mainContent'] = $this->load->view('user/edit', $this->data, true);
            $this->load->view('layout/main', $this->data);
        }
        else {
            redirect('/');
        }
        
        
    }
    
    private function _photoResize()
    {
        $loc = $this->upload->data();
		$loc = $loc['file_name'];
		
		// Default size
		$image['source_image'] = './photos/'.$loc;
		$image['width'] = 229;
		$image['height'] = 229;
		
		$this->load->library('image_lib', $image);

		$this->image_lib->resize();
		
		// Thumbnail size
        $image['new_image'] = './photos/thumb/'.$loc;
		$image['maintain_ratio'] = true;
		$image['width'] = 64;
		$image['height'] = 64;
        
        $this->image_lib->initialize($image); 
		$this->image_lib->resize();
        
        return $loc;
    }
    
    private function _photoUpload()
    {
		$config['upload_path'] = './photos/';
		$config['allowed_types'] = 'gif|jpg|bmp|png';
		
		$this->upload->initialize($config);
		return $this->upload->do_upload('photo');
    }
}
