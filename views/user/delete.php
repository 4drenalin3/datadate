<h2>Profiel verwijderen</h2>

<?php echo validation_errors(); 

echo form_open('user/deleteProfile'); ?>
<p>
   Weet je zeker dat je je profiel permanent wilt verwijderen? Je account met alle bijbehorende gegevens zal onherstelbaar worden gewist.
</p>


<p>
   <?php echo form_submit('delete', 'Ja, verwijder mijn account.'); ?>
</p>
<?php echo form_close(); ?>