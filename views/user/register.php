<h2>Registreer</h2>

<?php echo validation_errors();
if($success) echo 'Uw profiel is succesvol aangemaakt! U kunt nu inloggen door hierboven op inloggen te klikken.';
else 
{		

 	echo form_open_multipart('user/register'); ?>
 
 <p>
	<?php 
		echo form_label('Nickname: ', 'nickName');
		echo form_input('nickName', set_value('nickName'), 'id="nickName"');
	?>
</p>
<p>
	<?php 
		echo form_label('Naam: ', 'fullName');
		echo form_input('fullName', set_value('fullName'), 'id="fullName"');
	?>
</p>
<p>
	<?php 
		echo form_label('Password: ', 'password');
		echo form_password('password', null, 'id="password"');
	?>
</p>
<p>
	<?php 
		echo form_label('Email: ', 'email');
		echo form_input('email', set_value('email'), 'id="email"');
	?>
</p>
<p>
	<?php
		echo form_label('Geslacht: ', 'sex');
		echo form_dropdown('sex', array(NULL,'m'=>'Man','f'=>'Vrouw'), set_value('sex', 18), 'id="sex"');
	?>
</p>
<p>
	<?php
		echo form_label('Geboortedatum: ', 'birthDate');
		echo form_input('birthDate', set_value('birthDate'), 'id="birthDate"');
		echo ' (YYYY-MM-DD)';
	?>
</p>
<p>
	<?php
		echo form_label('Beschrijving: ', 'description');
		echo form_textarea('description', set_value('description'), 'id="description"');
	?>
</p>
<p>
	<?php
		echo form_label('Foto: ', 'photo');
    ?>
	<input type="file" name="photo" id="photo">
</p>
<p>
	<?php
		echo form_label('Voorkeur: ', 'sexPreference');
		echo form_dropdown('sexPreference', array(NULL, 'm'=>'Mannen', 'f'=>'Vrouwen', 'mf'=>'Mannen en vrouwen'), set_value('sexPreference'), 'id="sexPreference"');
	?>
</p>
<p>
	<?php		
		$ageRange = array_combine(range(18,100),range(18,100));
		echo form_label('Minimum leeftijd: ', 'minAge');
		echo form_dropdown('minAge', $ageRange, set_value('minAge'), 'id="minAge"');
?>
<br>
    <?php
		echo form_label(' Maximum leeftijd: ', 'maxAge');
		echo form_dropdown('maxAge', $ageRange, set_value('maxAge'), 'id="maxAge"');
	?>
</p>
<br />
<h3>Bepalen persoonlijkheid</h3>

<div class="personalityTest">
<?php
	$questions[1] = array('Ik geef de voorkeur aan grote groepen mensen, met een grote diversiteit.', 
       	    	          'Ik geef de voorkeur aan intieme bijeenkomsten met uitsluitend goede vrienden.', 
           	    	      'Ik zit er eigenlijk tussenin.');
	$questions[2] = array('Ik doe eerst, en dan denk ik.', 
       		              'Ik denk eerst, en dan doe ik.', 
               		      'Ik zit er eigenlijk tussenin.');
	$questions[3] = array('Ik ben makkelijk afgeleid, met minder aandacht voor een enkele specifieke taak.', 
       		              'Ik kan me goed focussen, met minder aandacht voor het grote geheel.', 
               		      'Ik zit er eigenlijk tussenin.');
	$questions[4] = array('Ik ben een makkelijke prater en ga graag uit.', 
       		              'Ik ben een goede luisteraar en meer een priv&eacute;-persoon.', 
               		      'Ik zit er eigenlijk tussenin.');
	$questions[5] = array('Als gastheer/-vrouw ben ik altijd het centrum van de belangstelling.', 
       		              'Als gastheer/-vrouw ben altijd achter de schermen bezig om te zorgen dat alles soepeltjes verloopt.', 
               		      'Ik zit er eigenlijk tussenin.');
	$questions[6] = array('Ik geef de voorkeur aan concepten en het leren op basis van associaties.', 
       		              'Ik geef de voorkeur aan observaties en het leren op basis van feiten.', 
               		      'Ik zit er eigenlijk tussenin.');
	$questions[7] = array('Ik heb een groot inbeeldingsvermogen en heb een globaal overzicht van een project.', 
       		              'Ik ben pragmatisch ingesteld en heb een gedetailleerd beeld van elke stap.', 
               		      'Ik zit er eigenlijk tussenin.');
	$questions[8] = array('Ik kijk naar de toekomst.', 
       		              'Ik houd mijn blik op het heden gericht.', 
               		      'Ik zit er eigenlijk tussenin.');
	$questions[9] = array('Ik houd van de veranderlijkheid in relaties en taken.', 
       		              'Ik houd van de voorspelbaarheid in relaties en taken.', 
               		      'Ik zit er eigenlijk tussenin.');
	$questions[10] = array('Ik overdenk een beslissing helemaal.', 
       		               'Ik beslis met mijn gevoel.', 
               		       'Ik zit er eigenlijk tussenin.');
	$questions[11] = array('Ik werk het beste met een lijst plussen en minnen.', 
       		               'Ik beslis op basis van de gevolgen voor mensen.', 
               		       'Ik zit er eigenlijk tussenin.');
	$questions[12] = array('Ik ben van nature kritisch.', 
       		               'Ik maak het mensen graag naar de zin.', 
               		       'Ik zit er eigenlijk tussenin.');
	$questions[13] = array('Ik ben eerder eerlijk dan tactisch.', 
       		               'Ik ben eerder tactisch dan eerlijk.', 
               		       'Ik zit er eigenlijk tussenin.');
	$questions[14] = array('Ik houd van vertrouwde situaties.', 
       		               'Ik houd van flexibele situaties.', 
               		       'Ik zit er eigenlijk tussenin.');
	$questions[15] = array('Ik plan alles, met een to-do lijstje in mijn hand.', 
       		               'Ik wacht tot er meerdere idee&euml;n opborrelen en kies dan on-the-fly wat te doen.', 
               		       'Ik zit er eigenlijk tussenin.');
	$questions[16] = array('Ik houd van het afronden van projecten.', 
       		               'Ik houd van het opstarten van projecten.', 
               		       'Ik zit er eigenlijk tussenin.');
	$questions[17] = array('Ik ervaar stress door een gebrek aan planning en abrupte wijzigingen.', 
       		               'Ik ervaar gedetailleerde plannen als benauwend en kijk uit naar veranderingen.', 
               		       'Ik zit er eigenlijk tussenin.');
	$questions[18] = array('Het is waarschijnlijker dat ik een doel bereik.', 
       		               'Het is waarschijnlijker dat ik een kans zie.', 
               		       'Ik zit er eigenlijk tussenin.');
	$questions[19] = array('"All play and no work maakt dat het project niet afkomt."', 
       		               '"All work and no play maakt je maar een saaie pief."', 
               		       'Ik zit er eigenlijk tussenin.');
foreach($questions as $num => $answers)
{
?>
<p>
	<?php
        foreach($answers as $i => $a)
        {
            $o = $i+1;
            echo form_radio('q'.$num, $o, FALSE, sprintf('id="q%d_%d" %s', $num, $o, set_radio('q'.$num, $o)));
            echo form_label($a, 'q'.$num.'_'.$o);
            echo '<br>';
        }
	?>
</p>
<?php
}
?>
</div>
<h3>Favoriete merken</h3>
<div class="brandSelect clearfix">
<?php
foreach($brands as $id => $name)
{
?>

    <p>
	<?php
            echo form_checkbox('brands[]', $id, false, sprintf('id="brands%d"', $id));
            echo form_label(html_escape($name), 'brands'.$id);
	?>
    </p>
<?php
}
?>
</div>
<p>
   <?php echo form_submit('submit', 'Registreer'); ?>
</p>
<?php echo form_close(); 
}?>
	