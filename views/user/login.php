<h2>Login</h2>

<?php echo validation_errors(); 

echo form_open('user/login'); ?>
<p>
   <?php 
      echo form_label('Email: ', 'email');
      echo form_input('email', set_value('email'), 'id="email"');
   ?>
</p>

<p>
   <?php 
      echo form_label('Wachtwoord:', 'password');
      echo form_password('password', null, 'id="password"');
   ?>
</p>

<p>
   <?php echo form_submit('submit', 'Login'); ?>
</p>
<?php echo form_close(); ?>