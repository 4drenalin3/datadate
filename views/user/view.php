<h2><?php echo html_escape($userInfo['nickName']); ?></h2>

<?php

$dob = strtotime($userInfo['birthDate']);
$age = ageFromDoB($dob);

$personality = $userInfo['I'] >= 50 ? 'I' : 'E';
$personality .= $userInfo['N'] >= 50 ? 'N' : 'S';
$personality .= $userInfo['T'] >= 50 ? 'T' : 'F';
$personality .= $userInfo['J'] >= 50 ? 'J' : 'P';

$personalityPref = $userInfo['pref_I'] >= 50 ? 'I' : 'E';
$personalityPref .= $userInfo['pref_N'] >= 50 ? 'N' : 'S';
$personalityPref .= $userInfo['pref_T'] >= 50 ? 'T' : 'F';
$personalityPref .= $userInfo['pref_J'] >= 50 ? 'J' : 'P';

$brandList = array();
if(!empty($userBrands))
    foreach(array_slice($userBrands, 0, 16) as $b) // only show first 16 brands
        $brandList[] = $brands[$b];
  
$avatarClasses = '';
      
if($userInfo['likesSess'] && $userInfo['sessLikes']) // only show these fields if we have a mutual match
    $privateFields = array('Naam' => html_escape($userInfo['fullName']),
                           'Email' => html_escape($userInfo['email']));

if($userInfo['likesSess']) $avatarClasses .= ' likes';
if($userInfo['sessLikes']) $avatarClasses .= ' liked';

$fields = array('Geslacht' => ($userInfo['sex'] === 'm' ? 'Man' : 'Vrouw'),
                'Geslachtsvoorkeur' => ($userInfo['sexPreference'] === 'mf' ? 'Man of vrouw' : 
											($userInfo['sexPreference'] === 'm' ? 'Man' : 'Vrouw')),   
                'Leeftijd' => sprintf('%d (%s)', $age, date('j F Y', $dob)),
                'Leeftijdsvoorkeur' => sprintf('%d - %d', $userInfo['minAge'], $userInfo['maxAge']),
                'Persoonlijkheid' => $personality,
                'Persoonlijkheidsvoorkeur' => $personalityPref,
                'Favoriete merken' => html_escape(implode(', ',$brandList)),
                'Beschrijving' => nl2br(html_escape($userInfo['description'])));
                
if($privateFields) $fields = array_merge($privateFields, $fields);
  
// only show avatar if viewing user is logged in, otherwise show default sex avatar
$avatar = (!empty($userInfo['photo']) && $loggedIn) ? $userInfo['photo'] : 'default_'.$userInfo['sex'].'.png';  
 
// only show like button if user is logged in and doesn't already like them
if($loggedIn && !$sameUser && !$userInfo['sessLikes']) 
{
    echo form_open(null, array('class' => 'likeForm'));
    echo form_submit('like', 'Like');
    echo form_close();
}

echo avatar($avatar, $avatarClasses);

foreach($fields as $name => $value)
{
    printf('<strong>%s</strong>: %s<br>', $name, $value);
}

if(!$loggedIn)
{
    printf('<br>In contact komen met %s? <a href="%s">Schrijf je dan nu in!</a>', html_escape($userInfo['nickName']), site_url('user/register'));
}
