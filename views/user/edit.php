<h2 class="inline">Profiel bewerken</h2> (<a href="<?php echo site_url('user/deleteProfile'); ?>"><img src="<?php echo base_url('img/delete_icon.png'); ?>" alt="Profiel verwijderen"> Profiel verwijderen</a>)

<?php echo validation_errors();
if($success) echo '<br>Uw profiel is succesvol gewijzigd!';		

 	echo form_open_multipart('user/edit'); ?>
<p>
	<?php 
		echo form_label('Nickname: ', 'nickName');
		echo form_input('nickName', set_value('nickName', $user->nickName), 'id="nickName"');
	?>
</p>
<p>
	<?php 
		echo form_label('Naam: ', 'fullName');
		echo form_input('fullName', set_value('fullName', $user->fullName), 'id="fullName"');
	?>
</p>
<p>
	<?php 
		echo form_label('Password: ', 'password');
		echo form_password('password', null, 'id="password"');
	?>
</p>
<p>
	<?php 
		echo form_label('Email: ', 'email');
		echo form_input('email', set_value('email'), 'id="email"');
	?>
</p>
<p>
	<?php
		echo form_label('Geslacht: ', 'sex');
		echo form_dropdown('sex', array(NULL,'m'=>'Man','f'=>'Vrouw'), set_value('sex', $user->sex), 'id="sex"');
	?>
</p>
<p>
	<?php
		echo form_label('Geboortedatum: ', 'birthDate');
		echo form_input('birthDate', set_value('birthDate', $user->birthDate), 'id="birthDate"');
		echo ' (YYYY-MM-DD)';
	?>
</p>
<p>
	<?php
		echo form_label('Beschrijving: ', 'description');
		echo form_textarea('description', set_value('description', $user->description), 'id="description"');
	?>
</p>
<p>
	<?php
		echo form_label('Foto: ', 'photo');
    ?>
	<input type="file" name="photo" id="photo">
</p>
<p>
	<?php
            echo form_checkbox('deletePhoto', 1, FALSE, 'id="deletePhoto"');
            echo form_label('Foto verwijderen', 'deletePhoto');
	?>
    </p>
<p>
	<?php
		echo form_label('Voorkeur: ', 'sexPreference');
		echo form_dropdown('sexPreference', array(NULL, 'm'=>'Mannen', 'f'=>'Vrouwen', 'mf'=>'Mannen en vrouwen'), set_value('sexPreference', $user->sexPreference), 'id="sexPreference"');
	?>
</p>
<p>
	<?php		
		$ageRange = array_combine(range(18,100),range(18,100));
		echo form_label('Minimum leeftijd: ', 'minAge');
		echo form_dropdown('minAge', $ageRange, set_value('minAge', $user->minAge), 'id="minAge"');
?>
<br>
    <?php
		echo form_label(' Maximum leeftijd: ', 'maxAge');
		echo form_dropdown('maxAge', $ageRange, set_value('maxAge',$user->maxAge), 'id="maxAge"');
	?>
</p>
<h2>Favoriete merken</h2>
<div class="brandSelect clearfix">
<?php
foreach($brands as $id => $name)
{
?>

    <p>
	<?php
            echo form_checkbox('brands[]', $id, isset($userBrands[$id]), sprintf('id="brands%d"', $id));
            echo form_label(html_escape($name), 'brands'.$id);
	?>
    </p>
<?php
}
?>
</div>

<p>
   <?php echo form_submit('submit', 'Wijzigen'); ?>
</p>
<?php echo form_close(); ?>
	