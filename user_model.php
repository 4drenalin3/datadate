<?php
class User_model extends CI_Model
{
    function __construct()
    {

    }
    
    public function register($data, $brands)
    {
        if($this->db->insert('users', $data))
        {
            if(!empty($brands))
            {
                $uid = $this->db->insert_id();
                foreach($brands as $brand)
                    $this->db->insert('userBrands', array('user' => $uid, 'brand' => $brand));
            }  
            
            return true;
        }
            
            
        return false;
    }

    public function checkUser($email, $password)
    {
        $qry = $this->db->where('email', $email)->where('password', sha1($password))->limit(1)->get('users');

        if ($qry->num_rows > 0)
            return $qry->row();
            
        return false;
    }
    
    public function get($id = false, $sessUid = false)
    {
        $qry = $this->db->select('users.*')->limit(1);
        
        
        if($id > 0) 
        {
            $qry->where('users.id', $id);
            
			// If we're logged in, check if we like or are liked by the user as well
            if($sessUid)
                $qry->select('l.likes AS likesSess, ll.likes AS sessLikes')
                    ->join('likes AS l', sprintf('users.id = l.user AND l.likes = %d', $sessUid), 'left')
                    ->join('likes AS ll', sprintf('users.id = ll.likes AND ll.user = %d', $sessUid), 'left');
        }
        
        return $qry->get('users')->row_array();
    }
    
    public function getRandom($amount) {
        return $this->db->order_by('RANDOM()')
                        ->limit($amount)
                        ->get('users')->result_array();
    }
    
    public function getBrands($uid)
    {
		// Retrieve for multiple users
        if(is_array($uid)){
            $qry = $this->db->where_in('user', $uid)->order_by('user')->get('userBrands')->result();
            if($qry)
                foreach($qry as $b)
                    $result[$b->user][] = $b->brand;
        }
        else // Or just one user
            $result = $this->db->where('user', $uid)->get('userBrands')->result();
            
        return $result;
    }
    
    public function addLike($liker, $likee)
    {
        $data = array('user' => $liker,
                      'likes' => $likee);
        $this->db->insert('likes', $data);
        
        $likerUser = $this->get($liker);
        $likeeUser = $this->get($likee);
        
		// Slightly adjust the user preference towards the liked user
        $dichotomies = array('pref_I' => $this->conf->alfa*$likerUser['pref_I'] + (1-$this->conf->alfa)*$likeeUser['I'],
                             'pref_N' => $this->conf->alfa*$likerUser['pref_N'] + (1-$this->conf->alfa)*$likeeUser['N'],
                             'pref_T' => $this->conf->alfa*$likerUser['pref_T'] + (1-$this->conf->alfa)*$likeeUser['T'],
                             'pref_J' => $this->conf->alfa*$likerUser['pref_J'] + (1-$this->conf->alfa)*$likeeUser['J']);
                             
        $this->db->update('users', $dichotomies, sprintf('id = %d', $liker));
           
    }
    
    public function update($userId, $data, $brands)
    {            
        $this->db->update('users', $data, sprintf('id = %d', $userId));    
        
        if(!empty($brands))
        {
            $this->db->delete('userBrands', array('user' => $userId));
            foreach($brands as $brand)
                $this->db->insert('userBrands', array('user' => $userId, 'brand' => $brand));
        }   
    }
    
    public function delete($userId)
    {            
        $this->db->delete('userBrands', array('user' => $userId));
        $this->db->delete('users', array('id' => $userId));
        $this->db->where('user', $userId)->or_where('likes', $userId)->delete('likes');
        
        if(!empty($brands))
        {
            $this->db->delete('userBrands', array('user' => $userId));
            foreach($brands as $brand)
                $this->db->insert('userBrands', array('user' => $userId, 'brand' => $brand));
        }   
    }
}
?>